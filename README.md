Tämä on node.js-pohjainen Internet of Things -sovellus, joka ottaa vastaan dataa kolmesta erityyppisestä sensorista. Sovellus on tehokkuusoptimoitu kuvitteelliselle sulautetulle järjestelmälle. Laajennettavuus on tällöin toissijaista.

Testiajo löytyy osoitteesta http://s3.eu-central-1.amazonaws.com/torava/iot/index.html