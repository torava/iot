var express = require('express'),
		app = express(),
		server = require('http').Server(app),
		io = require('socket.io')(server);

// http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
function MergeRecursive(obj1, obj2) {

  for (var p in obj2) {
    try {
      // Property in destination object set; update its value.
      if ( obj2[p].constructor==Object ) {
        obj1[p] = MergeRecursive(obj1[p], obj2[p]);

      } else {
        obj1[p] = obj2[p];

      }

    } catch(e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];

    }
  }

  return obj1;
}

server.listen(8080);

// datan alustusmäärittely
var init = {
	level: {
		max: 60
	},
	status: {},
	value: {
		max: 10,
		history_length: 50,
		values: []
	}
}

// data
var data = {
	level: {
		expense: 0
	},
	status: {
		begin_time: new Date().getTime()/1000,
		duration: 0
	},
	value: {
		expense: 0
	}
}

// void fetch_init_data() - luo satunnaista dataa 50 kertaa
function fetch_init_data() {
	var val;

	for (var i = 0; i < init.value.history_length; i++) {
		val = Math.floor(Math.random()*init.value.max);
		data.value.expense+= val;
		init.value.values.push(val);
	}

	data.status.value = true;
	data.level.value = init.level.max;
	data.value.value = val;
}
// void fetch_data() - noutaisi datan sensoreilta, mutta tässä luo satunnaista dataa
function fetch_data() {
	// simuloidaan level tyhjenemään maksimista
	var expense = Math.floor(Math.random()*10);
	// kasvatetaan kertymää
	data.level.expense+= expense;
	// asetetaan arvo
	data.level.value = data.level.value-expense;

	// täytetään level
	if (data.level.value <= 0) data.level.value = init.level.max;

	// arvotaan statuksen arvo
	if (Math.floor(Math.random()*5) == 0) {
		data.status.value = !data.status.value;
	}

	// mitataan statuksen päälläoloaikaa
	if (data.status.value) {
		var duration = new Date().getTime()/1000-data.status.begin_time;
		data.status.begin_time = new Date().getTime()/1000;
		data.status.duration+= duration;
	}

	// asetetaan valuen arvo
	data.value.value = Math.floor(Math.random()*init.value.max);
	// kasvatetaan kertymää
	data.value.expense+= data.value.value;

	// poistetaan 
	init.value.values.shift();
	init.value.values.push(data.value.value);
}
// data get_data() - palauttaa tallennetun datan
function get_data(initialization) {
	return initialization ? MergeRecursive(init, data) : data;
}

fetch_init_data();

// vastaanottaa yhteyskutsut
io.on('connection', function (socket) {
	// lähettää alustusdatan
	socket.emit('init', get_data(true));

	// lähetetään uutta dataa kerran 2 sekunnissa
	setInterval(function() {
		fetch_data();
		socket.emit('update', get_data());
	}, 2000);
});