// void fit() - sovittaa sovelluksen ikkunaan sopivaksi
function fit() {
	var w = d3.select('#container').style('width');
	document.getElementById('value-line').width = parseInt(w)-30;
}

// pääohjelma
(function() {

var gauge, smoothie, level_line, value_data, value_gauge,
	socket = io.connect('ws://localhost:8080'), // yhdistetään palvelimeen
	counter = 0, timeout;

// void update_fields(data) - päivittää kentät uudella datalla
function update_fields(data) {
	// jatketaan kuvaajan skrollaamista
	smoothie.start();

	// palautetaan tilailmoitus
	d3.select('#system-status').text('OK');

	// nollataan timeout interval
	counter = 0;
	window.clearInterval(timeout);
	// ilmoitetaan käyttäjää palvelimen aikaviipeestä, jos se ei vastaa 5 sekuntiin
	timeout = setInterval(function() {
		counter+= 5;
		d3.select('#system-status').text('Ei vastausta '+counter+' sekuntiin');
		smoothie.stop();
	}, 5000);

	// päivitetään arvot
	gauge.update(data.level.value);
	d3.select('#status').classed('on', data.status.value);
	value_gauge.load({columns: [['value', data.value.value]]});
	value_line.append(new Date().getTime(), data.value.value);

	// päivitetään kertymät
	d3.select('#level-expense').text(Math.round(data.level.expense/100,1));
	d3.select('#value-expense').text(Math.round(data.value.expense/100,1));

	// päivitetään kestot
	d3.select('#status-duration').attr('data-raw', data.status.duration);

	if (data.status.duration < 60) {
		d3.select('#status-duration').text(Math.round(data.status.duration));
		d3.select('#status-unit').text('sekuntia');
		d3.select('#status-minute-duration').text('');
		d3.select('#status-minute-unit').text('');
	}
	else {
		var minutes = Math.floor(data.status.duration/60),
			seconds = Math.round(data.status.duration)-minutes*60;
		if (seconds) {
			d3.select('#status-duration').text(seconds);
			d3.select('#status-unit').text('sekuntia');
		}
		else {
			d3.select('#status-duration').text('');
			d3.select('#status-unit').text('');
		}
		d3.select('#status-minute-duration').text(minutes);
		d3.select('#status-minute-unit').text('minuuttia');
	}
}

socket.on('init', function(init_data) {
	// asetetaan tilailmoitus
	d3.select('#system-status').text('OK');

	// määritetään valuen historiakuvaaja
	smoothie = new SmoothieChart({
				millisPerPixel: 40,
				minValue: 0,
				maxValue: 10,
				grid: {
					strokeStyle: '#8bd5ed',
					fillStyle:'transparent',
					strokeStyle:'rgba(119,119,119,0.53)',
					sharpLines:true,
					verticalSections:3,
					lineWidth: 1,
					millisPerLine: 10000,
				},
				labels:{fillStyle:'#000000'},
				timestampFormatter:SmoothieChart.timeFormatter
			}),
	// määritetään valuen historiakuvaaja
	value_line = new TimeSeries(),
	// määritetään valuen mittari
	value_gauge = c3.generate({
		bindto: '#value-gauge',
		data: {
			columns: [['value', init_data.value.value]],
			type: 'gauge'
		},
		gauge: {
			label: {
				format: function(value,ratio) {
					return value
				}
			},
			max: 10,
			units: ''
		},
		color: {
				pattern: ['#60B044', '#F6C600', '#F97600', '#FF0000'],
				threshold: {
						values: [0, 3, 6, 9]
				}
		}
	});


	// määritetään levelin mittari
	var config = liquidFillGaugeDefaultSettings();
	config.circleColor = "#41312F";
	config.textColor = "#1C080D";
	config.waveTextColor = "#9F877E";
	config.waveColor = "#351F0C";
	config.circleThickness = 0.2;
	config.displayPercent = false;
	config.waveAnimate = false;
	config.circleRoundness = 0.3;
	config.waveHeight = 0;
	config.maxValue = 60;

	gauge = loadLiquidFillGauge("level", init_data.level.value, config);

	// lisätään valuen historiakuvaajaan tietoa
	for (var i = 0; i < init_data.value.values.length; i++) {
		value_line.append(new Date().getTime()-i*1000, init_data.value.values[i]);
	}

	// päivitetään kentät määritysdatalla
	update_fields(init_data);

	// päivitetään kentät, kun palvelimelta tulee uutta dataa
	socket.on('update', function (data) {
		update_fields(data);
	});

	// määritetään valuen mittari
	smoothie.addTimeSeries(value_line, {strokeStyle: '#8bd5ed'});
	smoothie.streamTo(document.getElementById('value-line'), 2000);

	// sovitetaan sovellus ikkunaan
	fit();
});

})();